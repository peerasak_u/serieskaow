<?php
	require('HttpClient.class.php');
	
	header("Content-type: Application/json");

	$series_id = $_POST["id"];

	$client = new HttpClient('http://allnewservice.serviceseries.com', 80);

	$headers = array('User-Agent'=>'com.nsmsoftservice.series9ipad/3.0 (unknown, iPhone OS 6.0, iPad, Scale/2.000000)');

	$data = array('type'=> 'serielink',
		'ps'=> '100',
		'id'=> $series_id);

	$client->post('/newversion/Series9Service.ashx', $data, $headers);

	$content = $client->getContent();

	echo $content;

?>